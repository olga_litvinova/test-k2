import {OnDestroy} from '@angular/core';
import {Subject} from 'rxjs';

export abstract class ObservableComponent implements OnDestroy {
  private ngUnsubscribeSubject$ = new Subject();

  protected get ngUnsubscribe$() {
    return this.ngUnsubscribeSubject$.asObservable();
  }

  ngOnDestroy(): void {
    this.ngUnsubscribeSubject$.next();
    this.ngUnsubscribeSubject$.complete();
  }
}
