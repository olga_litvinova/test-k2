import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {LayoutComponent, NotFoundComponent} from './components';

@NgModule({
  imports: [
    TranslateModule,
    CommonModule,
    RouterModule,
  ],
  declarations: [
    NotFoundComponent,
    LayoutComponent,
  ],
  exports: [
    NotFoundComponent,
    LayoutComponent,
  ]
})
export class LayoutModule {
}
