import {Pipe, PipeTransform} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Pipe({
  name: 'yesNo'
})
export class YesNoPipe implements PipeTransform {
  constructor(private translate: TranslateService) {
  }

  transform(value: any): any {
    return this.translate.instant(value ? 'boolean.yes' : 'boolean.no');
  }
}
