import {ITodoItem} from '../interfaces/todo-item.interface';

export interface ITodoState {
  items: ITodoItem[];
}
