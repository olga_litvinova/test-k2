import {createFeatureSelector, createSelector} from '@ngrx/store';
import {TodoActions, TodoActionTypes} from './todo.actions';
import {ITodoState} from './todo.state';

export type State = ITodoState;

export const initialState: State = {
  items: []
};

export const FeatureName = 'todo';
export const getTodoState = createFeatureSelector<ITodoState>(FeatureName);
export const getTodoItems = createSelector(getTodoState, (state: State) => state.items);

export function reducer(state = initialState, action: TodoActions): State {
  // console.log(action);
  switch (action.type) {
    case TodoActionTypes.TodoListSet:
      return {
        ...state,
        items: action.payload
      };
    case TodoActionTypes.TodoItemAdd:
      return {
        ...state,
        items: state.items.concat([action.payload])
      };
    case TodoActionTypes.TodoItemRemove:
      return {
        ...state,
        items: state.items.filter(item => item.id !== action.payload.id)
      };
    case TodoActionTypes.TodoItemUpdate:
      const items = [...state.items];
      const idx = items.findIndex(item => item.id === action.payload.id);

      if (idx !== -1) {
        items[idx] = action.payload;
      }

      return {
        ...state,
        items: items
      };
    default:
      return state;
  }
}
