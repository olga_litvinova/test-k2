import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {distinct, filter, map, mergeMap} from 'rxjs/operators';
import {ITodoState} from './todo.state';
import {TodoActionTypes, TodoItemAdd, TodoItemRemove, TodoItemSave, TodoItemUpdate, TodoListSet} from './todo.actions';
import {TodoService} from '../services/todo.service';

@Injectable()
export class TodoEffects {
  @Effect()
  todoListGet$: Observable<any> = this.actions$
    .pipe(
      ofType(TodoActionTypes.TodoListGet),
      mergeMap(() => this.service.list()),
      filter(response => response.isSuccess),
      map(response => new TodoListSet(response.data))
    );

  @Effect()
  todoItemAdd$: Observable<any> = this.actions$
    .pipe(
      ofType(TodoActionTypes.TodoItemAdd),
      map((action: TodoItemAdd) => action.payload),
      mergeMap(item => this.service.add(item)),
      filter(response => response.isSuccess),
      map(response => new TodoItemUpdate(response.data))
    );

  @Effect()
  todoItemRemove$: Observable<any> = this.actions$
    .pipe(
      ofType(TodoActionTypes.TodoItemRemove),
      map((action: TodoItemRemove) => action.payload),
      distinct(),
      mergeMap(item => this.service.remove(item)),
      filter(response => response.isSuccess),
      map(response => new TodoItemRemove(response.data))
    );

  @Effect()
  todoItemSave$: Observable<any> = this.actions$
    .pipe(
      ofType(TodoActionTypes.TodoItemSave),
      map((action: TodoItemSave) => action.payload),
      mergeMap(item => this.service.save(item)),
      filter(response => response.isSuccess),
      map(response => new TodoItemUpdate(response.data))
    );

  constructor(
    private store: Store<ITodoState>,
    private actions$: Actions,
    private service: TodoService,
  ) {
  }
}
