import {Action} from '@ngrx/store';
import {ITodoItem} from '../interfaces/todo-item.interface';

export enum TodoActionTypes {
  TodoListGet = 'todo/list/get',
  TodoListSet = 'todo/list/set',
  TodoItemSave = 'todo/item/save',
  TodoItemAdd = 'todo/item/add',
  TodoItemUpdate = 'todo/item/update',
  TodoItemRemove = 'todo/item/remove'
}

export class TodoListGet implements Action {
  readonly type = TodoActionTypes.TodoListGet;

  constructor(public payload?: any) {
  }
}

export class TodoListSet implements Action {
  readonly type = TodoActionTypes.TodoListSet;

  constructor(public payload: ITodoItem[]) {
  }
}

export class TodoItemSave implements Action {
  readonly type = TodoActionTypes.TodoItemSave;

  constructor(public payload: ITodoItem) {
  }
}

export class TodoItemAdd implements Action {
  readonly type = TodoActionTypes.TodoItemAdd;

  constructor(public payload: ITodoItem) {
  }
}

export class TodoItemRemove implements Action {
  readonly type = TodoActionTypes.TodoItemRemove;

  constructor(public payload: ITodoItem) {
  }
}

export class TodoItemUpdate implements Action {
  readonly type = TodoActionTypes.TodoItemUpdate;

  constructor(public payload: ITodoItem) {
  }
}

export type TodoActions = TodoListGet | TodoListSet | TodoItemSave | TodoItemAdd | TodoItemRemove | TodoItemUpdate;
