import {Injectable} from '@angular/core';
import {ITodoItem} from '../interfaces/todo-item.interface';
import {Observable, of} from 'rxjs';
import {fromPromise} from 'rxjs/internal-compatibility';

export interface ITodoServiceBaseResponse {
  isSuccess: boolean;
}

export interface ITodoServiceDataResponse<T> extends ITodoServiceBaseResponse {
  data: T;
}

export interface ITodoServiceListResponse extends ITodoServiceDataResponse<ITodoItem[]> {
}

export interface ITodoServiceItemResponse extends ITodoServiceDataResponse<ITodoItem> {
}

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  private minTimeout = 5;
  private maxTimeout = 10;
  private items: ITodoItem[] = [{
    id: '9b1deb4d-3b7d-4bad-9bdd-2b0d7b3dcb6d',
    title: 'title 1',
    created: new Date()
  }, {
    id: '1b9d6bcd-bbfd-4b2d-9b5d-ab8dfbbd4bed',
    title: 'title 2',
    created: new Date()
  }];

  public list(): Observable<ITodoServiceListResponse> {
    return of({
      data: [...this.items],
      isSuccess: true
    });
  }

  public add(item: ITodoItem): Observable<ITodoServiceItemResponse> {
    const timeout = this.getRandomTimeout();
    const promise = new Promise<ITodoServiceItemResponse>((resolve) => {
      console.log(`service.add -> "${item.title}" (timeout: ${timeout}s)`);

      const itemToAdd: ITodoItem = {
        ...item,
      };

      this.items.push(item);

      setTimeout(() => {
        console.log(`service.add <- "${item.title}"`);
        itemToAdd.created = new Date();

        resolve({
          data: {
            ...itemToAdd,
            isModified: false
          },
          isSuccess: true,
        });
      }, timeout * 1000);
    });

    return fromPromise<ITodoServiceItemResponse>(promise);
  }

  public remove(item: ITodoItem): Observable<ITodoServiceItemResponse> {
    const timeout = this.getRandomTimeout();
    const promise = new Promise<ITodoServiceItemResponse>((resolve) => {
      console.log(`service.remove -> "${item.title}" (timeout: ${timeout}s)`);

      setTimeout(() => {
        console.log(`service.remove <- "${item.title}"`);

        const idx = this.items.findIndex(it => it.id === item.id);
        this.items = this.items.filter(it => it.id !== item.id);

        resolve({
          data: item,
          isSuccess: idx !== -1,
        });
      }, timeout * 1000);
    });

    return fromPromise<ITodoServiceItemResponse>(promise);
  }

  public save(item: ITodoItem): Observable<ITodoServiceItemResponse> {
    const timeout = this.getRandomTimeout();
    const promise = new Promise<ITodoServiceItemResponse>((resolve) => {
      console.log(`service.save -> "${item.title}" (timeout: ${timeout}s)`);

      setTimeout(() => {
        console.log(`service.save <- "${item.title}"`);

        const itemToSave = {
          ...item,
          updated: new Date()
        };

        const idx = this.items.findIndex(it => it.id === item.id);
        if (idx !== -1) {
          this.items[idx] = itemToSave;
        }

        resolve({
          data: {
            ...itemToSave,
            isModified: false
          },
          isSuccess: idx !== -1,
        });
      }, timeout * 1000);
    });

    return fromPromise<ITodoServiceItemResponse>(promise);
  }

  private getRandomTimeout() {
    return Math.floor(Math.random() * (this.maxTimeout - this.minTimeout + 1)) + this.minTimeout;
  }
}
