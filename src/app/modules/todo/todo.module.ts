import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {TodoListComponent} from './components/todo-list';
import {TodoComponent} from './todo.component';
import {TodoRouting} from './todo.routing';
import {TodoEditComponent} from './components/todo-edit';
import * as fromState from './store';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {TodoEffects} from './store/todo.effects';
import {YesNoPipe} from './pipes/yesno.pipe';

@NgModule({
  imports: [
    EffectsModule.forFeature([TodoEffects]),
    StoreModule.forFeature(fromState.FeatureName, fromState.reducer),
    TranslateModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    TodoRouting,
  ],
  declarations: [
    TodoComponent,
    TodoListComponent,
    TodoEditComponent,
    YesNoPipe,
  ]
})
export class TodoModule {
}
