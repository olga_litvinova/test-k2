export interface ITodoItem {
  id: string;
  title: string;
  created?: Date;
  updated?: Date;
  isComplete?: boolean;
  isModified?: boolean;
}
