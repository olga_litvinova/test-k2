import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TodoListComponent} from './components/todo-list';
import {TodoComponent} from './todo.component';
import {TodoEditComponent} from './components/todo-edit';

export const routes: Routes = [{
  path: '',
  component: TodoComponent,
  children: [
    {path: '', redirectTo: 'list', pathMatch: 'full'},
    {path: 'list', component: TodoListComponent},
    {path: 'add', component: TodoEditComponent},
    {path: 'edit/:id', component: TodoEditComponent},
  ],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule
  ],
  exports: [
    RouterModule
  ]
})

export class TodoRouting {
}
