import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ObservableComponent} from '../../../shared/components';
import {ITodoItem} from '../../interfaces/todo-item.interface';
import {takeUntil} from 'rxjs/operators';
import {getTodoItems, ITodoState, TodoItemRemove, TodoItemSave, TodoItemUpdate, TodoListGet} from '../../store';
import {Store} from '@ngrx/store';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-todo-list-component',
  templateUrl: './todo-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TodoListComponent extends ObservableComponent implements OnInit {
  items: ITodoItem[] = [];

  constructor(
    private store: Store<ITodoState>,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private changeDetector: ChangeDetectorRef,
  ) {
    super();
  }

  ngOnInit(): void {
    this.store.select(getTodoItems)
      .pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe(items => {
        this.items = ([...items] || []).sort(this.itemCompare);
        this.changeDetector.detectChanges();
      });
  }

  onAddItem() {
    this.router.navigate(['../add'], {relativeTo: this.activatedRoute});
  }

  onRemoveItem(item: ITodoItem) {
    this.store.dispatch(new TodoItemRemove(item));
  }

  onCompleteItem(item: ITodoItem) {
    this.store.dispatch(new TodoItemUpdate({
      ...item,
      isModified: true
    }));
    this.store.dispatch(new TodoItemSave({
      ...item,
      isComplete: true
    }));
  }

  private itemCompare(valueA: ITodoItem, valueB: ITodoItem) {
    if (!valueA && !valueB) {
      return 0;
    }

    return ('' + valueA.title).localeCompare(valueB.title) * -1;
  }
}
