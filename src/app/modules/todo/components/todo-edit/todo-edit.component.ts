import {ChangeDetectionStrategy, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {getTodoItems, ITodoState, TodoItemAdd, TodoItemSave, TodoItemUpdate} from '../../store';
import {ITodoItem} from '../../interfaces/todo-item.interface';
import {v4 as uuid} from 'uuid';
import {ObservableComponent} from '../../../shared/components';
import {takeUntil, withLatestFrom} from 'rxjs/operators';

@Component({
  selector: 'app-todo-edit-component',
  templateUrl: './todo-edit.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TodoEditComponent extends ObservableComponent implements OnInit {
  @ViewChild('inputElement', {static: true}) inputElement: ElementRef;
  item: ITodoItem = <ITodoItem>{};
  form: FormGroup;

  constructor(
    private store: Store<ITodoState>,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {
    super();
    this.form = this.fb.group({
      title: this.fb.control(null, Validators.required)
    });
  }

  ngOnInit(): void {
    this.activatedRoute.params
      .pipe(
        takeUntil(this.ngUnsubscribe$),
        withLatestFrom(this.store.select(getTodoItems))
      )
      .subscribe(([params, items]) => {
        if (params && params['id']) {
          const item = items.find(it => it.id === params.id);
          if (item) {
            this.item = item;
            this.form.patchValue({
              title: this.item.title
            });
          }
        }
      });

    if (this.inputElement && this.inputElement.nativeElement) {
      setTimeout(() => {
        this.inputElement.nativeElement.focus();
      }, 50);
    }
  }

  onSave() {
    if (!!this.item.id) {
      this.store.dispatch(new TodoItemUpdate({
        ...this.item,
        title: this.form.value.title,
        isModified: true
      }));
      this.store.dispatch(new TodoItemSave({
        ...this.item,
        title: this.form.value.title
      }));
    } else {
      this.store.dispatch(new TodoItemAdd({
        id: this.item.id ? this.item.id : uuid(),
        title: this.form.value.title,
        isModified: true
      }));
    }
    this.router.navigateByUrl('/todo/list');
  }

  onCancel() {
    this.router.navigateByUrl('/todo/list');
  }
}
