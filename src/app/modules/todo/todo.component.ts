import {ChangeDetectionStrategy, Component} from '@angular/core';
import {ITodoState, TodoListGet} from './store';
import {Store} from '@ngrx/store';

@Component({
  selector: 'app-todo-component',
  templateUrl: './todo.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TodoComponent {

  constructor(
    private store: Store<ITodoState>,
  ) {
    this.store.dispatch(new TodoListGet());
  }
}
