import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {LayoutComponent, NotFoundComponent} from './modules/layout/components';

const routes: Routes = [{
  path: '',
  component: LayoutComponent,
  children: [
    {
      path: '',
      redirectTo: 'todo',
      pathMatch: 'full'
    }, {
      path: 'todo',
      loadChildren: () => import('./modules/todo/todo.module').then(m => m.TodoModule)
    }
  ]
}, {
  path: 'not-found',
  component: NotFoundComponent
}, {
  path: '**',
  redirectTo: 'not-found'
}];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
    }),
    CommonModule,
  ],
  exports: [
    RouterModule
  ]
})
export class AppRouting {
}
